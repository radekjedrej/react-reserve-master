import React from 'react';
import axios from 'axios';


function Home({ products }) {
  console.log(products);
  
  // React.useEffect(() => {
  //   getProducts()
  // }, [])
  // async function getProducts() {
  //   // console.log(response.data);
  // }

  return <>home</>;
}

Home.getInitialProps = async () => {
  // fetch data on server
  const url = 'http://localhost:3000/api/products';
  const response = await axios.get(url);
  // return response data as object
  return { products: response.data }
  // note: this object will be merged with existing data
}

export default Home;

